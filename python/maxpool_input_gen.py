import torch
import torch.nn.functional as F
import numpy as np
import sys

def create_random_8bit_int_4d_array(dim):
    # 주어진 크기만큼의 8-bit 4차원 정수 배열을 랜덤한 값으로 생성
    return np.random.randint(0, 256, size=(dim[0], dim[1], dim[2], dim[3]), dtype=np.uint8)

def load_8bit_int_array_from_file(filename, dim):
    # 파일에서 8-bit 정수 배열을 불러옴
    return np.fromfile(filename, dtype=np.uint8).reshape(dim[0], dim[1], dim[2], dim[3])

def save_8bit_int_array_to_file(filename, data):
    # 파일에 8-bit 정수 배열을 저장
    data.tofile(filename)

def main():
    if len(sys.argv) != 8:
        dim = [1, 3, 28, 28]
        kernel_size = 3
        pad_size = 1
        stride_size = 1
    else:
        dim = [int(arg) for arg in sys.argv[1:5]]
        kernel_size = int(sys.argv[5])
        pad_size = int(sys.argv[6])
        stride_size = int(sys.argv[7])

    # 주어진 길이만큼의 random한 8-bit integer array 생성
    array1 = create_random_8bit_int_4d_array(dim)

    # 배열을 input.bin 파일에 저장
    array2 = array1.transpose(0, 2, 3, 1)
    save_8bit_int_array_to_file("input1.bin", array2)

    # 배열을 텐서로 변환
    array1 = torch.tensor(array1).to(torch.float32)

    # 최대 풀링 연산: PyTorch는 NCHW 입력을 받으며, 타겟 연산 C코드는 NHWC 입력을 받음
    result = F.max_pool2d(array1, kernel_size=kernel_size, padding=pad_size, stride=stride_size).permute(0, 2, 3, 1)

    # 텐서를 배열로 변환, 반올림
    result = np.round(result.numpy()).astype(np.uint8)

    # 결과를 출력 파일에 저장
    save_8bit_int_array_to_file("golden.bin", result)

if __name__ == "__main__":
    main()
