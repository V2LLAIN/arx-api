#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include "include/API.h"

typedef struct {
    float *image_data;
    unsigned char *label_data;
} MNISTData;

float *load_32bit_float_array_from_file(const char *filename, size_t *size);
uint8_t *load_8bit_uint_array_from_file(const char *filename, size_t *size);
void save_32bit_int_to_file(int data, const char *filename);

int mnist_datasets(MNISTData *dataset, const char *image_path, const char *label_path, int num_images);
void print_image_fp32(float *image_data, int width, int height);
void print_image_i8(uint8_t *image_data, int width, int height);

int main(int argc, char **argv) {

    const char* output_names[] = {
        "scale1_float32", "zero_point1_uint8",

        "w_quantize1_uint8", "w_scale1_float32", "w_zero_point1_uint8",
        "scale2_float32", "zero_point2_uint8",
        "conv_bias1_uint8", "conv_bias1_scale_float32", "conv_bias1_zero_point_uint8",

        "w_quantize2_uint8", "w_scale2_float32", "w_zero_point2_uint8",
        "scale3_float32", "zero_point3_uint8",
        "conv_bias2_int32", "conv_bias2_scale_float32", "conv_bias2_zero_point_uint8",

        "w_quantize3_uint8", "w_scale3_float32", "w_zero_point3_uint8",
        "scale4_float32", "zero_point4_uint8",

        "w_quantize4_uint8", "w_scale4_float32", "w_zero_point4_uint8",
        "scale5_float32", "zero_point5_uint8"
    };

    float *data_0; uint8_t *data_1;

    uint8_t *data_2; float *data_3; uint8_t *data_4;
    float *data_5; uint8_t *data_6;
    uint8_t *data_7; float *data_8; uint8_t *data_9;

    uint8_t *data_10; float *data_11; uint8_t *data_12;
    float *data_13; uint8_t *data_14;
    uint8_t *data_15; float *data_16; uint8_t *data_17;

    uint8_t *data_18; float *data_19; uint8_t *data_20;
    float *data_21; uint8_t *data_22;

    uint8_t *data_23; float *data_24; uint8_t *data_25;
    float *data_26; uint8_t *data_27;

    size_t sizes[28];

    char filepaths[28][256];
    for (int i = 0; i < 28; ++i) {
        snprintf(filepaths[i], sizeof(filepaths[i]), "%s/%s.bin", argv[1], output_names[i]);
    }

    data_0 = load_32bit_float_array_from_file(filepaths[0], &sizes[0]);
    data_1 = load_8bit_uint_array_from_file(filepaths[1], &sizes[1]);


    data_2 = load_8bit_uint_array_from_file(filepaths[2], &sizes[2]);
    data_3 = load_32bit_float_array_from_file(filepaths[3], &sizes[3]);
    data_4 = load_8bit_uint_array_from_file(filepaths[4], &sizes[4]);

    data_5 = load_32bit_float_array_from_file(filepaths[5], &sizes[5]);
    data_6 = load_8bit_uint_array_from_file(filepaths[6], &sizes[6]);

    data_7 = load_8bit_uint_array_from_file(filepaths[7], &sizes[7]);
    data_8 = load_32bit_float_array_from_file(filepaths[8], &sizes[8]);
    data_9 = load_8bit_uint_array_from_file(filepaths[9], &sizes[9]);


    data_10 = load_8bit_uint_array_from_file(filepaths[10], &sizes[10]);
    data_11 = load_32bit_float_array_from_file(filepaths[11], &sizes[11]);
    data_12 = load_8bit_uint_array_from_file(filepaths[12], &sizes[12]);

    data_13 = load_32bit_float_array_from_file(filepaths[13], &sizes[13]);
    data_14 = load_8bit_uint_array_from_file(filepaths[14], &sizes[14]);

    data_15 = load_8bit_uint_array_from_file(filepaths[15], &sizes[15]);
    data_16 = load_32bit_float_array_from_file(filepaths[16], &sizes[16]);
    data_17 = load_8bit_uint_array_from_file(filepaths[17], &sizes[17]);


    data_18 = load_8bit_uint_array_from_file(filepaths[18], &sizes[18]);
    data_19 = load_32bit_float_array_from_file(filepaths[19], &sizes[19]);
    data_20 = load_8bit_uint_array_from_file(filepaths[20], &sizes[20]);

    data_21 = load_32bit_float_array_from_file(filepaths[21], &sizes[21]);
    data_22 = load_8bit_uint_array_from_file(filepaths[22], &sizes[22]);


    data_23 = load_8bit_uint_array_from_file(filepaths[23], &sizes[23]);
    data_24 = load_32bit_float_array_from_file(filepaths[24], &sizes[24]);
    data_25 = load_8bit_uint_array_from_file(filepaths[25], &sizes[25]);

    data_26 = load_32bit_float_array_from_file(filepaths[26], &sizes[26]);
    data_27 = load_8bit_uint_array_from_file(filepaths[27], &sizes[27]);


    unsigned char *data_2_nhwc = (unsigned char *)malloc(8*1*5*5 * sizeof(unsigned char));
    transpose_i8(data_2, data_2_nhwc, 8, 1, 5, 5,
                 8, 5, 5, 1,
                 0, 2, 3, 1);
    unsigned char *data_10_nhwc = (unsigned char *)malloc(16*8*5*5 * sizeof(unsigned char));
    transpose_i8(data_10, data_10_nhwc, 16, 8, 5, 5,
                 16, 5, 5, 8,
                 0, 2, 3, 1);

    MNISTData mnist_test[10000];

    if (mnist_datasets(mnist_test, "./data/images", "./data/labels", 10000) != 0) {
        fprintf(stderr, "Failed to load MNIST data\n");
        return 1;
    }

    uint8_t *output1 = (uint8_t *)malloc(1*1*28*28 * sizeof(uint8_t));
    uint8_t *output2 = (uint8_t *)malloc(1*28*28*8 * sizeof(uint8_t));
    uint8_t *output3 = (uint8_t *)malloc(1*14*14*8 * sizeof(uint8_t));
    uint8_t *output4 = (uint8_t *)malloc(1*14*14*16 * sizeof(uint8_t));
    uint8_t *output5 = (uint8_t *)malloc(1*4*4*16 * sizeof(uint8_t));
    uint8_t *output6 = (uint8_t *)malloc(1*4*4*16 * sizeof(uint8_t));
    uint8_t *output7 = (uint8_t *)malloc(1*10 * sizeof(uint8_t));
    float *output8 = (float *)malloc(1*10 * sizeof(float));

    int count = 0;
    for (int i=0; i<100; ++i) {
        float *input_image = mnist_test[i].image_data;
        unsigned char *output_label = mnist_test[i].label_data;

        quantize(input_image, output1, 784, data_0, data_1);

        convolution_i8_scale(output1, data_0, data_1,
                            data_2_nhwc, data_3, data_4,
                            data_7, data_8, data_9,
                            output2, data_5, data_6,
                            1, 28, 28, 1,
                            8, 5, 5,
                            2, 1, 0, 1,
                            28, 28);

        maxpool_i8(output2, output3,
                    1, 28, 28, 8,
                    2, 2, 0, 2);

        convolution_i8_scale(output3, data_5, data_6,
                        data_10_nhwc, data_11, data_12,
                        data_15, data_16, data_17,
                        output4, data_13, data_14,
                        1, 14, 14, 8,
                        16, 5, 5,
                        2, 1, 0, 1,
                        14, 14);

        maxpool_i8(output4, output5,
                1, 14, 14, 16,
                3, 3, 0, 3);

        transpose_i8(output5, output6, 1, 4, 4, 16,
                    1, 16, 4, 4,
                    0, 3, 1, 2);

        fullyconnected_i8_scale(output6, data_13, data_14,
                    data_18, data_19, data_20,
                    data_23, data_24, data_25,
                    output7, data_26, data_27,
                    1, 256, 256, 10,
                    1, 1, 10);

        dequantize(output7, output8, 10, data_26, data_27, 1, 0);

        int maxIndex = 0;
        float maxValue = output8[0];
        for (int i = 1; i < 10; ++i) {
            if (output8[i] > maxValue) {
                maxValue = output8[i];
                maxIndex = i;
            }
        }
        if(output_label[0] == maxIndex) {
            count = count + 1;
            // printf("전체:%d 정답:%d\n", i+1, count);
        }
    }
    count = (count / 10) * 10;
    save_32bit_int_to_file(count, "output.bin");
    free(data_2_nhwc);
    free(data_10_nhwc);
    free(output1);
    free(output2);
    free(output3);
    free(output4);
    free(output5);
    free(output6);
    free(output7);
    free(output8);
    return 0;
}

float *load_32bit_float_array_from_file(const char *filename, size_t *size) {
    FILE *fp = fopen(filename, "rb");
    if (fp == NULL) {
        perror("Error opening file");
        return NULL;
    }

    fseek(fp, 0, SEEK_END);
    long file_size = ftell(fp);
    fseek(fp, 0, SEEK_SET);

    *size = file_size / sizeof(float);
    float *array = (float *)malloc(file_size);
    if (array == NULL) {
        perror("Memory allocation failed");
        fclose(fp);
        return NULL;
    }

    fread(array, sizeof(float), *size, fp);
    fclose(fp);
    return array;
}


uint8_t *load_8bit_uint_array_from_file(const char *filename, size_t *size) {
    FILE *fp = fopen(filename, "rb");
    if (fp == NULL) {
        perror("Error opening file");
        return NULL;
    }

    fseek(fp, 0, SEEK_END);
    long file_size = ftell(fp);
    fseek(fp, 0, SEEK_SET);

    *size = file_size / sizeof(uint8_t);
    uint8_t *array = (uint8_t *)malloc(file_size);
    if (array == NULL) {
        perror("Memory allocation failed");
        fclose(fp);
        return NULL;
    }

    fread(array, sizeof(uint8_t), *size, fp);
    fclose(fp);
    return array;
}

void save_32bit_int_to_file(int data, const char *filename) {
    FILE *fp = fopen(filename, "wb");
    if (fp == NULL) {
        perror("Error opening file");
        return;
    }

    fwrite(&data, sizeof(data), 1, fp);
    fclose(fp);
}

int mnist_datasets(MNISTData *dataset, const char *image_path, const char *label_path, int num_images) {
    char image_file[256], label_file[256];
    size_t image_size, label_size;

    for (int i = 0; i < num_images; i++) {
        snprintf(image_file, sizeof(image_file), "%s/image%d.bin", image_path, i);
        snprintf(label_file, sizeof(label_file), "%s/label%d.bin", label_path, i);

        dataset[i].image_data = load_32bit_float_array_from_file(image_file, &image_size);
        if (dataset[i].image_data == NULL) {
            fprintf(stderr, "Failed to load image data for index %d\n", i);
            return 1;
        }

        dataset[i].label_data = load_8bit_uint_array_from_file(label_file, &label_size);
        if (dataset[i].label_data == NULL) {
            fprintf(stderr, "Failed to load label data for index %d\n", i);
            return 1;
        }
    }

    return 0;
}

void print_image_fp32(float *image_data, int width, int height) {
    for (int y = 0; y < height; y++) {
        for (int x = 0; x < width; x++) {
            float pixel = image_data[y * width + x];
            char ch = (pixel > 0.5) ? '#' : '.'; // 임계값에 따라 문자 선택
            printf("%c ", ch);
        }
        printf("\n");
    }
}

void print_image_i8(uint8_t *image_data, int width, int height) {
    for (int y = 0; y < height; y++) {
        for (int x = 0; x < width; x++) {
            uint8_t pixel = image_data[y * width + x];
            char ch = (pixel == 1) ? '*' : '.'; // 임계값에 따라 문자 선택
            printf("%c ", ch);
        }
        printf("\n");
    }
}

