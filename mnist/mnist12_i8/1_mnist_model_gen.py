import os
import urllib.request

def download_onnx_model(url, output_path):
    # model 폴더가 없으면 생성
    os.makedirs(os.path.dirname(output_path), exist_ok=True)

    try:
        # URL에서 파일 다운로드
        with urllib.request.urlopen(url) as response, open(output_path, 'wb') as out_file:
            data = response.read()  # 바이트 데이터 읽기
            out_file.write(data)  # 파일에 저장
        print(f"모델 다운로드 성공: {output_path}")
    except Exception as e:
        print(f"오류 발생: {e}")

# mnist-12-int8 모델 다운로드
url_mnist_12_int8 = "https://github.com/onnx/models/raw/main/validated/vision/classification/mnist/model/mnist-12-int8.onnx"
path_mnist_12_int8 = "./model/mnist-12-int8.onnx"
download_onnx_model(url_mnist_12_int8, path_mnist_12_int8)
