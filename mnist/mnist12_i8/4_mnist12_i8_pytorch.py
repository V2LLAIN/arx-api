import os
import torch
import numpy as np
import torch.nn.functional as F

def dequantize(input, scale, zero_point):
    return ((input.astype(np.int32) - np.int32(zero_point)) * scale).astype(np.float32)

def quantize(input, scale, zero_point):
    return np.clip(np.round(input / scale + zero_point), 0, 255).astype(np.uint8)

def QLinearConv(x, x_scale, x_zero_point, w, w_scale, w_zero_point, y_scale, y_zero_point, B, B_scale, B_zero_point):
    deq_x = dequantize(x, x_scale, x_zero_point)
    deq_w = dequantize(w, w_scale.reshape(-1,1,1,1), w_zero_point.reshape(-1,1,1,1))
    deq_y = F.conv2d(torch.from_numpy(deq_x), torch.from_numpy(deq_w), stride = 1, padding = 2)
    if B is not None:
        deq_B = dequantize(B, B_scale, B_zero_point)
        deq_y = deq_y + torch.from_numpy(deq_B.reshape(1,-1,1,1))
    q_y = quantize(deq_y.numpy(), y_scale, y_zero_point)
    return q_y

def QLinearMatMul(a, a_scale, a_zero_point, b, b_scale, b_zero_point, y_scale, y_zero_point):
    deq_a = dequantize(a, a_scale, a_zero_point)
    deq_b = dequantize(b, b_scale, b_zero_point)
    deq_y = torch.matmul(torch.from_numpy(deq_a), torch.from_numpy(deq_b))
    q_y = quantize(deq_y.numpy(), y_scale, y_zero_point)
    return q_y

def QLinearAdd(a, a_scale, a_zero_point, b, b_scale, b_zero_point, y_scale, y_zero_point):
    deq_a = dequantize(a, a_scale, a_zero_point)
    deq_b = dequantize(b, b_scale, b_zero_point)
    deq_y = deq_a + deq_b.reshape(1,-1)
    q_y = quantize(deq_y, y_scale, y_zero_point)
    return q_y

def QLinearMatMulAdd(a, a_scale, a_zero_point, b, b_scale, b_zero_point, bias, bias_scale, bias_zero_point, y_scale, y_zero_point):
    deq_a = dequantize(a, a_scale, a_zero_point)
    deq_b = dequantize(b, b_scale, b_zero_point)
    deq_bias = dequantize(bias, bias_scale, bias_zero_point)
    deq_y = torch.matmul(torch.from_numpy(deq_a), torch.from_numpy(deq_b))
    deq_y = deq_y + torch.from_numpy(deq_bias)
    q_y = quantize(deq_y.numpy(), y_scale, y_zero_point)
    return q_y

# 파일에서 32-bit 실수 배열을 불러옴
def load_32bit_float_array_from_file(filename):
    return np.fromfile(filename, dtype=np.float32)

# 파일에서 8-bit 부호없는 정수 배열을 불러옴
def load_8bit_uint_array_from_file(filename):
    return np.fromfile(filename, dtype=np.uint8)

# 파일에 배열을 저장
def save_array_to_file(filename, data):
    data.tofile(filename)

output_names = [
    'scale1_float32', 'zero_point1_uint8',
    'w_quantize1_uint8', 'w_scale1_float32', 'w_zero_point1_uint8', 
    'scale2_float32', 'zero_point2_uint8', 
    'conv_bias1_uint8', 'conv_bias1_scale_float32', 'conv_bias1_zero_point_uint8'
    'w_quantize2_uint8', 'w_scale2_float32', 'w_zero_point2_uint8', 
    'scale3_float32', 'zero_point3_uint8', 
    'conv_bias2_uint8', 'conv_bias2_scale_float32', 'conv_bias2_zero_point_uint8'
    'w_quantize3_uint8', 'w_scale3_float32', 'w_zero_point3_uint8', 
    'scale4_float32', 'zero_point4_uint8',
    'w_quantize4_uint8', 'w_scale4_float32', 'w_zero_point4_uint8', 
    'scale5_float32', 'zero_point5_uint8'
]


# 경로 설정
param_dir = './param'
image_path = './data/images'
label_path = './data/labels'
mnist_test = []

# 이미지와 라벨을 불러와 리스트에 추가
for i in range(10000):
    image_file = os.path.join(image_path, f'image{i}.bin')
    label_file = os.path.join(label_path, f'label{i}.bin')

    # 이미지 데이터 불러오기 및 형태 변환
    image_data = load_32bit_float_array_from_file(image_file).reshape(1, 1, 28, 28)

    # 라벨 데이터 불러오기
    label_data = load_8bit_uint_array_from_file(label_file)

    # 이미지 데이터와 라벨 데이터를 튜플 형태로 리스트에 추가
    mnist_test.append((image_data, label_data))

# bin 파일에서 데이터 불러오기
# 연산(1)
bin_path11 = os.path.join(param_dir, 'scale1_float32' + '.bin')
weight11 = load_32bit_float_array_from_file(bin_path11).reshape(1)
bin_path12 = os.path.join(param_dir, 'zero_point1_uint8' + '.bin')
weight12 = load_8bit_uint_array_from_file(bin_path12).reshape(1)

# 연산(1)
bin_path21 = os.path.join(param_dir, 'w_quantize1_uint8' + '.bin')
weight21 = load_8bit_uint_array_from_file(bin_path21).reshape(8, 1, 5, 5)
bin_path22 = os.path.join(param_dir, 'w_scale1_float32' + '.bin')
weight22 = load_32bit_float_array_from_file(bin_path22).reshape(8)
bin_path23 = os.path.join(param_dir, 'w_zero_point1_uint8' + '.bin')
weight23 = load_8bit_uint_array_from_file(bin_path23).reshape(8)

bin_path24 = os.path.join(param_dir, 'scale2_float32' + '.bin')
weight24 = load_32bit_float_array_from_file(bin_path24).reshape(1)
bin_path25 = os.path.join(param_dir, 'zero_point2_uint8' + '.bin')
weight25 = load_8bit_uint_array_from_file(bin_path25).reshape(1)

bin_path26 = os.path.join(param_dir, 'conv_bias1_uint8' + '.bin')
weight26 = load_8bit_uint_array_from_file(bin_path26).reshape(8)
bin_path27 = os.path.join(param_dir, 'conv_bias1_scale_float32' + '.bin')
weight27 = load_32bit_float_array_from_file(bin_path27).reshape(8)
bin_path28 = os.path.join(param_dir, 'conv_bias1_zero_point_uint8' + '.bin')
weight28 = load_8bit_uint_array_from_file(bin_path28).reshape(8)

# 연산(2)
bin_path31 = os.path.join(param_dir, 'w_quantize2_uint8' + '.bin')
weight31 = load_8bit_uint_array_from_file(bin_path31).reshape(16, 8, 5, 5)
bin_path32 = os.path.join(param_dir, 'w_scale2_float32' + '.bin')
weight32 = load_32bit_float_array_from_file(bin_path32).reshape(16)
bin_path33 = os.path.join(param_dir, 'w_zero_point2_uint8' + '.bin')
weight33 = load_8bit_uint_array_from_file(bin_path33).reshape(16)

bin_path34 = os.path.join(param_dir, 'scale3_float32' + '.bin')
weight34 = load_32bit_float_array_from_file(bin_path34).reshape(1)
bin_path35 = os.path.join(param_dir, 'zero_point3_uint8' + '.bin')
weight35 = load_8bit_uint_array_from_file(bin_path35).reshape(1)

bin_path36 = os.path.join(param_dir, 'conv_bias2_uint8' + '.bin')
weight36 = load_8bit_uint_array_from_file(bin_path36).reshape(16)
bin_path37 = os.path.join(param_dir, 'conv_bias2_scale_float32' + '.bin')
weight37 = load_32bit_float_array_from_file(bin_path37).reshape(16)
bin_path38 = os.path.join(param_dir, 'conv_bias2_zero_point_uint8' + '.bin')
weight38 = load_8bit_uint_array_from_file(bin_path38).reshape(16)

# 연산(3)
bin_path41 = os.path.join(param_dir, 'w_quantize3_uint8' + '.bin')
weight41 = load_8bit_uint_array_from_file(bin_path41).reshape(256, 10)
bin_path42 = os.path.join(param_dir, 'w_scale3_float32' + '.bin')
weight42 = load_32bit_float_array_from_file(bin_path42).reshape(10)
bin_path43 = os.path.join(param_dir, 'w_zero_point3_uint8' + '.bin')
weight43 = load_8bit_uint_array_from_file(bin_path43).reshape(10)

bin_path44 = os.path.join(param_dir, 'scale4_float32' + '.bin')
weight44 = load_32bit_float_array_from_file(bin_path44).reshape(1)
bin_path45 = os.path.join(param_dir, 'zero_point4_uint8' + '.bin')
weight45 = load_8bit_uint_array_from_file(bin_path45).reshape(1)

# 연산(4)
bin_path51 = os.path.join(param_dir, 'w_quantize4_uint8' + '.bin')
weight51 = load_8bit_uint_array_from_file(bin_path51).reshape(1, 10)
bin_path52 = os.path.join(param_dir, 'w_scale4_float32' + '.bin')
weight52 = load_32bit_float_array_from_file(bin_path52).reshape(1)
bin_path53 = os.path.join(param_dir, 'w_zero_point4_uint8' + '.bin')
weight53 = load_8bit_uint_array_from_file(bin_path53).reshape(1)

bin_path54 = os.path.join(param_dir, 'scale5_float32' + '.bin')
weight54 = load_32bit_float_array_from_file(bin_path54).reshape(1)
bin_path55 = os.path.join(param_dir, 'zero_point5_uint8' + '.bin')
weight55 = load_8bit_uint_array_from_file(bin_path55).reshape(1)


count = 0 # 정확도 측정
for i in range(10000):

    # 테스트 데이터셋에서 하나의 이미지 선택
    input_image, output_label = mnist_test[i]
    output1 = quantize(input_image, weight11, weight12)

    output2 = QLinearConv(output1, weight11, weight12,
                        weight21, weight22, weight23,
                        weight24, weight25, 
                        weight26, weight27, weight28)

    output3 = F.max_pool2d(torch.from_numpy(output2), kernel_size=2, stride=2, padding=0).numpy()

    output4 = QLinearConv(output3, weight24, weight25,
                        weight31, weight32, weight33,
                        weight34, weight35, 
                        weight36, weight37, weight38)

    output5 = F.max_pool2d(torch.from_numpy(output4), kernel_size=3, stride=3, padding=0).numpy()

    output6 = output5.reshape(1, 256)
    
    output7 = QLinearMatMulAdd(output6, weight34, weight35,
                            weight41, weight42, weight43,
                            weight51, weight52, weight53,
                            weight44, weight45)

    golden = dequantize(output7, weight44, weight45)

    if (output_label == np.argmax(golden[0])):
        count = count + 1
        print('전체:', i+1, ' 정답:', count)


