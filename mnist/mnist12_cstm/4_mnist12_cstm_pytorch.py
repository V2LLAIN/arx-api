import os
import torch
import numpy as np
import torch.nn.functional as F

# 파일에서 32-bit 실수 배열을 불러옴
def load_32bit_float_array_from_file(filename):
    return np.fromfile(filename, dtype=np.float32)

# 파일에서 8-bit 부호없는 정수 배열을 불러옴
def load_8bit_uint_array_from_file(filename):
    return np.fromfile(filename, dtype=np.uint8)

# 파일에 배열을 저장
def save_array_to_file(filename, data):
    data.tofile(filename)

def dequantize(input, scale, zero_point):
    return ((input.astype(np.int32) - np.int32(zero_point)) * scale).astype(np.float32)

def quantize(input, scale, zero_point):
    return np.clip(np.round(input / scale + zero_point), 0, 255).astype(np.uint8)

def QLinearConvAdd(x, x_scale, x_zero_point, w, w_scale, w_zero_point, bias, bias_scale, bias_zero_point, y_scale, y_zero_point):
    deq_x = dequantize(x, x_scale, x_zero_point)
    deq_w = dequantize(w, w_scale.reshape(-1,1,1,1), w_zero_point)
    deq_bias = dequantize(bias, bias_scale.reshape(1,-1,1,1), bias_zero_point)
    deq_y = F.conv2d(torch.from_numpy(deq_x), torch.from_numpy(deq_w), stride = 1, padding = 2)
    deq_y = deq_y + torch.from_numpy(deq_bias)
    q_y = quantize(deq_y.numpy(), y_scale, y_zero_point)
    return q_y

def QLinearMatMulAdd(a, a_scale, a_zero_point, b, b_scale, b_zero_point, bias, bias_scale, bias_zero_point, y_scale, y_zero_point):
    deq_a = dequantize(a, a_scale, a_zero_point)
    deq_b = dequantize(b, b_scale, b_zero_point)
    deq_bias = dequantize(bias, bias_scale, bias_zero_point)
    deq_y = torch.matmul(torch.from_numpy(deq_a), torch.from_numpy(deq_b))
    deq_y = deq_y + torch.from_numpy(deq_bias)
    q_y = quantize(deq_y.numpy(), y_scale, y_zero_point)
    return q_y

def QLinearReLU(x, x_zero_point):
    x_move = x.astype(np.int32) - x_zero_point
    x_relu = F.relu(torch.from_numpy(x_move)).numpy()
    x_result = x_relu + x_zero_point
    return x_result

# 파라미터 경로 설정
param_dir = './param'

# bin 파일에서 데이터 불러오기
bin_path12 = os.path.join(param_dir, 'q_weight1.bin')
q_weight1 = load_8bit_uint_array_from_file(bin_path12).reshape(8, 1, 5, 5)
bin_path13 = os.path.join(param_dir, 'q_scale1.bin')
q_scale1 = load_32bit_float_array_from_file(bin_path13).reshape(8, 1, 1, 1)
bin_path14 = os.path.join(param_dir, 'q_zero_point1.bin')
q_zero_point1 = load_8bit_uint_array_from_file(bin_path14).reshape(8, 1, 1, 1)


bin_path22 = os.path.join(param_dir, 'q_weight2.bin')
q_weight2 = load_8bit_uint_array_from_file(bin_path22).reshape(8, 1, 1)
bin_path23 = os.path.join(param_dir, 'q_scale2.bin')
q_scale2 = load_32bit_float_array_from_file(bin_path23)
bin_path24 = os.path.join(param_dir, 'q_zero_point2.bin')
q_zero_point2 = load_8bit_uint_array_from_file(bin_path24)


bin_path32 = os.path.join(param_dir, 'q_weight3.bin')
q_weight3 = load_8bit_uint_array_from_file(bin_path32).reshape(16, 8, 5, 5)
bin_path33 = os.path.join(param_dir, 'q_scale3.bin')
q_scale3 = load_32bit_float_array_from_file(bin_path33).reshape(16, 1, 1, 1)
bin_path34 = os.path.join(param_dir, 'q_zero_point3.bin')
q_zero_point3 = load_8bit_uint_array_from_file(bin_path34).reshape(16, 1, 1, 1)


bin_path42 = os.path.join(param_dir, 'q_weight4.bin')
q_weight4 = load_8bit_uint_array_from_file(bin_path42).reshape(16, 1, 1)
bin_path43 = os.path.join(param_dir, 'q_scale4.bin')
q_scale4 = load_32bit_float_array_from_file(bin_path43)
bin_path44 = os.path.join(param_dir, 'q_zero_point4.bin')
q_zero_point4 = load_8bit_uint_array_from_file(bin_path44)


bin_path52 = os.path.join(param_dir, 'q_weight5.bin')
q_weight5 = load_8bit_uint_array_from_file(bin_path52).reshape(16, 4, 4, 10).reshape(256, 10)
bin_path53 = os.path.join(param_dir, 'q_scale5.bin')
q_scale5 = load_32bit_float_array_from_file(bin_path53).reshape(1, 10)
bin_path54 = os.path.join(param_dir, 'q_zero_point5.bin')
q_zero_point5 = load_8bit_uint_array_from_file(bin_path54).reshape(1, 10)


bin_path62 = os.path.join(param_dir, 'q_weight6.bin')
q_weight6 = load_8bit_uint_array_from_file(bin_path62).reshape(1, 10)
bin_path63 = os.path.join(param_dir, 'q_scale6.bin')
q_scale6 = load_32bit_float_array_from_file(bin_path63)
bin_path64 = os.path.join(param_dir, 'q_zero_point6.bin')
q_zero_point6 = load_8bit_uint_array_from_file(bin_path64)


bin_path70 = os.path.join(param_dir, 'y_scale0.bin')
y_scale0 = load_32bit_float_array_from_file(bin_path70)
bin_path71 = os.path.join(param_dir, 'y_scale1.bin')
y_scale1 = load_32bit_float_array_from_file(bin_path71)
bin_path72 = os.path.join(param_dir, 'y_scale2.bin')
y_scale2 = load_32bit_float_array_from_file(bin_path72)
bin_path73 = os.path.join(param_dir, 'y_scale3.bin')
y_scale3 = load_32bit_float_array_from_file(bin_path73)


bin_path74 = os.path.join(param_dir, 'y_zero_point0.bin')
y_zero_point0 = load_8bit_uint_array_from_file(bin_path74)
bin_path75 = os.path.join(param_dir, 'y_zero_point1.bin')
y_zero_point1 = load_8bit_uint_array_from_file(bin_path75)
bin_path76 = os.path.join(param_dir, 'y_zero_point2.bin')
y_zero_point2 = load_8bit_uint_array_from_file(bin_path76)
bin_path77 = os.path.join(param_dir, 'y_zero_point3.bin')
y_zero_point3 = load_8bit_uint_array_from_file(bin_path77)

# 데이터셋 경로 설정
image_path = './data/images'
label_path = './data/labels'
mnist_test = []

# 이미지와 라벨을 불러와 리스트에 추가
for i in range(10000):
    image_file = os.path.join(image_path, f'image{i}.bin')
    label_file = os.path.join(label_path, f'label{i}.bin')

    # 이미지 데이터 불러오기 및 형태 변환
    image_data = load_32bit_float_array_from_file(image_file).reshape(1, 1, 28, 28)

    # 라벨 데이터 불러오기
    label_data = load_8bit_uint_array_from_file(label_file)

    # 이미지 데이터와 라벨 데이터를 튜플 형태로 리스트에 추가
    mnist_test.append((image_data, label_data))

count = 0
for i in range(1):

    # 테스트 데이터셋에서 하나의 이미지 선택
    input_image, output_label = mnist_test[i]
    # 양자화
    output0 = quantize(input_image, y_scale0, y_zero_point0)

    # 합성곱
    output1 = QLinearConvAdd(output0, y_scale0, y_zero_point0,
                            q_weight1, q_scale1, q_zero_point1,
                            q_weight2, q_scale2, q_zero_point2,
                            y_scale1, y_zero_point1)
    # 활성화 함수
    output2 = QLinearReLU(output1, y_zero_point1)

    # 최대 풀링
    output3 = F.max_pool2d(torch.from_numpy(output2), kernel_size=2, stride=2, padding=0).numpy()

    # 합성곱
    output4 = QLinearConvAdd(output3, y_scale1, y_zero_point1,
                            q_weight3, q_scale3, q_zero_point3,
                            q_weight4, q_scale4, q_zero_point4,
                            y_scale2, y_zero_point2)
    # 활성화 함수
    output5 = QLinearReLU(output4, y_zero_point2)
    # 최대 풀링
    output6 = F.max_pool2d(torch.from_numpy(output5).type(torch.float32) , kernel_size=3, stride=3, padding=0).type(torch.uint8).numpy()
    # 행렬곱
    output7 = output6.reshape(1, 256)



    output8 = QLinearMatMulAdd(output7, y_scale2, y_zero_point2,
                            q_weight5, q_scale5, q_zero_point5,
                            q_weight6, q_scale6, q_zero_point6,
                            y_scale3, y_zero_point3)
    # 역양자화
    output9 = dequantize(output8, y_scale3, y_zero_point3)

    for i in range(10):
        if(output9.flatten()[i]):
            print(output9.flatten()[i])

    # 추론 검증
    if (output_label == np.argmax(output9)):
        count = count + 1
        print('전체:', i+1, ' 정답:', count)


