#ifndef ONES_MATH_H
#define ONES_MATH_H


float ones_exp(float x, int terms);
float ones_round(float number);
float ones_floorf(float x);


#endif
