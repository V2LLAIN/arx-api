//
// Created by User on 2023-10-07.
//
#include "include/API.h"
#include "include/ONES_MATH.h"
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv) {
    FILE *fp;
    float *input1, *param1;
    long size;
    // read arguments
    if (argc == 3) {
        // argv[1] is the input0 file
        fp = fopen(argv[1], "rb");
        if (fp == NULL) {
            printf("Error opening file\n");
            return 1;
        }

        // read binary file and store in memory
        fseek(fp, 0, SEEK_END);
        long fsize = ftell(fp);
        size = fsize / 4;
        fseek(fp, 0, SEEK_SET);
        input1 = malloc(fsize);
        fread(input1, fsize, 4, fp);
        fclose(fp);

        // argv[2] is the input1 file
        fp = fopen(argv[2], "rb");
        if (fp == NULL) {
            printf("Error opening file\n");
            return 1;
        }

        fseek(fp, 0, SEEK_END);
        fsize = ftell(fp);
        fseek(fp, 0, SEEK_SET);
        param1 = malloc(fsize);
        fread(param1, fsize, 4, fp);
        fclose(fp);
    }
    else if (argc != 1) {
        printf("Invalid number of arguments\n");
    }

    else {
        // data size = 1024
        long fsize = 1024 * 4;
        size = fsize / 4;
        input1 = malloc(fsize);
        param1 = malloc(2);
        // initialize input0 and input1 with random data
        for (int i = 0; i < size; ++i) {
            input1[i] = (float) rand();
        }
        float min_input1 = input1[0];
        float max_input1 = input1[0];
        for (int i = 0; i < size; ++i) {
            if (min_input1 > input1[i])
                min_input1 = input1[i];
            if (max_input1 < input1[i])
                max_input1 = input1[i];
        }
        param1[0] = (max_input1 - min_input1) / 255.0;
        param1[1] = 0 - ones_round(min_input1 / param1[0]);
    }
    float scale1[] = {param1[0]};
    unsigned char offset1[] = {param1[1]};

    unsigned char *output = malloc(size);
    quantize(input1, output, size, scale1, offset1);

    fp = fopen("output.bin", "wb");
    fwrite(output, size, 1, fp);
    fclose(fp);

    return 0;
}
