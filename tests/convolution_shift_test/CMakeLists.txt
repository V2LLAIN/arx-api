add_executable(arx_convolution_shift_test
        convolution_shift_test.c)


add_custom_command(
        TARGET arx_convolution_shift_test POST_BUILD
        COMMAND python3 ${CMAKE_HOME_DIRECTORY}/python/convolution_shift_input_gen.py 1024
)

target_link_libraries(arx_convolution_shift_test PUBLIC arx_api)

add_arxapi_test(NAME arx_convolution_shift_test COMMAND ${CMAKE_CURRENT_BINARY_DIR}/arx_convolution_shift_test USE_SH 1 PARAMS "${CMAKE_CURRENT_BINARY_DIR}/input.bin ${CMAKE_CURRENT_BINARY_DIR}/kernel.bin ${CMAKE_CURRENT_BINARY_DIR}/bias.bin ${CMAKE_CURRENT_BINARY_DIR}/arguments.bin" 
        USE_DIFF 1 DIFF_TARGET ${CMAKE_CURRENT_BINARY_DIR}/golden.bin)