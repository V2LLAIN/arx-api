//
// Created by User on 2023-10-07.
//
#include "include/API.h"
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv) {
    FILE *fp;
    unsigned char *input, *kernel, *bias, *arguments;
    long size;
    // read arguments
    if (argc == 5) {
        // argv[1] is the input tensor file
        fp = fopen(argv[1], "rb");
        if (fp == NULL) {
            printf("Error opening file\n");
            return 1;
        }

        // read binary file and store in memory
        fseek(fp, 0, SEEK_END);
        long fsize = ftell(fp);
        size = fsize;
        fseek(fp, 0, SEEK_SET);
        input = malloc(fsize);
        fread(input, fsize, 1, fp);
        fclose(fp);

        // argv[2] is the kernel tensor file
        fp = fopen(argv[2], "rb");
        if (fp == NULL) {
            printf("Error opening file\n");
            return 1;
        }

        // read binary file and store in memory
        fseek(fp, 0, SEEK_END);
        fsize = ftell(fp);
        size = fsize;
        fseek(fp, 0, SEEK_SET);
        kernel = malloc(fsize);
        fread(kernel, fsize, 1, fp);
        fclose(fp);

        // argv[3] is the bias tensor file
        fp = fopen(argv[3], "rb");
        if (fp == NULL) {
            printf("Error opening file\n");
            return 1;
        }

        // read binary file and store in memory
        fseek(fp, 0, SEEK_END);
        fsize = ftell(fp);
        size = fsize;
        fseek(fp, 0, SEEK_SET);
        bias = malloc(fsize);
        fread(bias, fsize, 1, fp);
        fclose(fp);

        // argv[4] is the arguments file
        fp = fopen(argv[4], "rb");
        if (fp == NULL) {
            printf("Error opening file\n");
            return 1;
        }

        // read binary file and store in memory
        fseek(fp, 0, SEEK_END);
        fsize = ftell(fp);
        size = fsize;
        fseek(fp, 0, SEEK_SET);
        arguments = malloc(fsize);
        fread(arguments, fsize, 1, fp);
        fclose(fp);
    }
    else if (argc != 1) {
        printf("Invalid number of arguments\n");
    }

    else {
        // unneccesary?
        // data size = 1024
        size = 1024;
        input = malloc(size);

        // initialize input with random data
        for (int i = 0; i < 1024; i++) {
            input[i] = (char) rand();
        }
    }


    // convolution input
    unsigned char out_size = arguments[0] * arguments[10] * arguments[11] * arguments[4];
    unsigned char *output = (unsigned char*) malloc(out_size * sizeof(unsigned char));

    convolution_i8_shift(input, kernel, bias, output, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4], arguments[5], arguments[6], arguments[7], arguments[8], 1, 1, arguments[9], arguments[10], arguments[11]);
    
    // write output to file
    fp = fopen("output.bin", "wb");
    fwrite(output, out_size, 1, fp);
    fclose(fp);

    free(input);
    free(kernel);
    free(bias);
    free(output);
    return 0;
}
