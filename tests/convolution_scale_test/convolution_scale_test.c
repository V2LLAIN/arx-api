//
// Created by User on 2023-10-07.
//

// #define INPUT_SCALE 1.2
// #define INPUT_OFFSET 1
// #define KERNEL_SCALE 1.3
// #define KERNEL_OFFSET 1
// #define BIAS_SCALE 1.1
// #define BIAS_OFFSET 1
// #define OUTPUT_SCALE 1.4
// #define OUTPUT_OFFSET 1
#define STRIDE_SIZE 1
#define PADDING_SIZE 1

#include "include/API.h"
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv) {
    FILE *fp;
    unsigned char *input, *kernel, *bias, *arguments;
    long size;
    // read arguments
    if (argc == 5) {
        // argv[1] is the input tensor file
        fp = fopen(argv[1], "rb");
        if (fp == NULL) {
            printf("Error opening file\n");
            return 1;
        }

        // read binary file and store in memory
        fseek(fp, 0, SEEK_END);
        long fsize = ftell(fp);
        size = fsize;
        fseek(fp, 0, SEEK_SET);
        input = malloc(fsize);
        fread(input, fsize, 1, fp);
        fclose(fp);

        // argv[2] is the kernel tensor file
        fp = fopen(argv[2], "rb");
        if (fp == NULL) {
            printf("Error opening file\n");
            return 1;
        }

        // read binary file and store in memory
        fseek(fp, 0, SEEK_END);
        fsize = ftell(fp);
        size = fsize;
        fseek(fp, 0, SEEK_SET);
        kernel = malloc(fsize);
        fread(kernel, fsize, 1, fp);
        fclose(fp);

        // argv[3] is the bias tensor file
        fp = fopen(argv[3], "rb");
        if (fp == NULL) {
            printf("Error opening file\n");
            return 1;
        }

        // read binary file and store in memory
        fseek(fp, 0, SEEK_END);
        fsize = ftell(fp);
        size = fsize;
        fseek(fp, 0, SEEK_SET);
        bias = malloc(fsize);
        fread(bias, fsize, 1, fp);
        fclose(fp);

        // argv[4] is the arguments file
        fp = fopen(argv[4], "rb");
        if (fp == NULL) {
            printf("Error opening file\n");
            return 1;
        }

        // read binary file and store in memory
        fseek(fp, 0, SEEK_END);
        fsize = ftell(fp);
        size = fsize;
        fseek(fp, 0, SEEK_SET);
        arguments = malloc(fsize);
        fread(arguments, fsize, 1, fp);
        fclose(fp);
    }
    else if (argc != 1) {
        printf("Invalid number of arguments\n");
    }

    else {
        // unneccesary?
        // data size = 1024
        size = 1024;
        input = malloc(size);

        // initialize input with random data
        for (int i = 0; i < 1024; i++) {
            input[i] = (char) rand();
        }
    }
    

    float INPUT_SCALE[] = {1.2};
    unsigned char INPUT_OFFSET[] = {1};
    float KERNEL_SCALE[] = {1.3};
    unsigned char KERNEL_OFFSET[] = {1};
    float BIAS_SCALE[] = {1.1};
    unsigned char BIAS_OFFSET[] = {1};
    float OUTPUT_SCALE[] = {1.4};
    unsigned char OUTPUT_OFFSET[] = {1};

    // convolution input
    unsigned char out_size = arguments[0] * arguments[17] * arguments[18] * arguments[4];
    unsigned char *output = (unsigned char*) malloc(out_size * sizeof(unsigned char));

    convolution_i8_scale(input, INPUT_SCALE, INPUT_OFFSET, kernel, KERNEL_SCALE, KERNEL_OFFSET, 
                         bias, BIAS_SCALE, BIAS_OFFSET, output, OUTPUT_SCALE, OUTPUT_OFFSET,
                         arguments[0], arguments[1], arguments[2], arguments[3], arguments[4], arguments[5], arguments[6],
                         arguments[15], arguments[16], 1, 1, arguments[17], arguments[18]);

    
    // write output to file
    fp = fopen("output.bin", "wb");
    fwrite(output, out_size, 1, fp);
    fclose(fp);

    free(input);
    free(kernel);
    free(output);

    return 0;
}
