//
// Created by User on 2024-01-10.
//
#include "include/API.h"
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv) {
    FILE *fp;
    unsigned char *input0;
    long size;
    // read arguments
    if (argc == 2) {
        // argv[1] is the input0 file
        fp = fopen(argv[1], "rb");
        if (fp == NULL) {
            printf("Error opening file\n");
            return 1;
        }

        // read binary file and store in memory
        fseek(fp, 0, SEEK_END);
        long fsize = ftell(fp);
        size = fsize;
        fseek(fp, 0, SEEK_SET);
        input0 = malloc(fsize);
        fread(input0, fsize, 1, fp);
        fclose(fp);

    }
    else if (argc != 1) {
        printf("Invalid number of arguments\n");
    }

    else {
        // data size = 1024
        size = 1024;
        input0 = malloc(size);

        // initialize input0 and input1 with random data
        for (int i = 0; i < 1024; i++) {
            input0[i] = (char) rand();
        }
    }

    // transpose_i8 input0
    unsigned char *output = malloc(size);
    transpose_i8(input0, output, 1, 3, 28, 28, 1, 28, 28, 3, 0, 2, 3, 1);

    // write output to file
    fp = fopen("output.bin", "wb");
    fwrite(output, size, 1, fp);
    fclose(fp);

    return 0;
}
