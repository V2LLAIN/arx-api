//
// Created by User on 2023-10-07.
//
#include "include/API.h"
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv) {
    FILE *fp;
    unsigned char *input0, *input1;
    long size;
    // read arguments
    if (argc == 3) {
        // argv[1] is the input0 file
        fp = fopen(argv[1], "rb");
        if (fp == NULL) {
            printf("Error opening file\n");
            return 1;
        }

        // read binary file and store in memory
        fseek(fp, 0, SEEK_END);
        long fsize = ftell(fp);
        size = fsize;
        fseek(fp, 0, SEEK_SET);
        input0 = malloc(fsize);
        fread(input0, fsize, 1, fp);
        fclose(fp);

        // argv[2] is the input1 file
        fp = fopen(argv[2], "rb");
        if (fp == NULL) {
            printf("Error opening file\n");
            return 1;
        }

        fseek(fp, 0, SEEK_END);
        fsize = ftell(fp);

        if(fsize != size) {
            printf("Error: input0 and input1 must have the same size\n");
            return 1;
        }

        fseek(fp, 0, SEEK_SET);
        input1 = malloc(fsize);
        fread(input1, fsize, 1, fp);
        fclose(fp);
    }
    else if (argc != 1) {
        printf("Invalid number of arguments\n");
    }

    else {
        // data size = 1024
        size = 1024;
        input0 = malloc(size);
        input1 = malloc(size);

        // initialize input0 and input1 with random data
        for (int i = 0; i < 1024; i++) {
            input0[i] = (char) rand();
            input1[i] = (char) rand();
        }
    }

    float input0_scale[] = {1.0}; unsigned char input0_offset[] = {0.0};
    float input1_scale[] = {1.0}; unsigned char input1_offset[] = {0.0};
    float output_scale[] = {1.0}; unsigned char output_offset[] = {0.0};

    // elemmax_i8 input0 and input1
    unsigned char *output = malloc(size);
    elemmax_i8(input0, input0_scale, input0_offset, input1, input1_scale, input1_offset, output, output_scale, output_offset, size);

    // write output to file
    fp = fopen("output.bin", "wb");
    fwrite(output, size, 1, fp);
    fclose(fp);

    return 0;
}
