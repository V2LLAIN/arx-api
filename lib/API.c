//
// Created by User on 2023-10-07.
// Being written by Sangheon Lee from 2024-01-08.

#include <stdlib.h>

#include "API.h"
#include "ONES_MATH.h"
#include <stdio.h>

// system setting
#define SYSTEM_32 0
#define SYSTEM_64 1

#if SYSTEM_32
    #define INTEGER_TYPE int
#elif SYSTEM_64
    #define INTEGER_TYPE long long int
#else
    #error "Unknown Architecture"
#endif

int convolution_i8_shift(unsigned char* input, unsigned char* kernel, unsigned char *bias, unsigned char* output,
                         unsigned char N, unsigned char H, unsigned char W, unsigned char C, unsigned char KN, unsigned char KH, unsigned char KW,
                         unsigned char pad_size, unsigned char stride_size, bool doRelu, bool doBias, unsigned char shift, unsigned char out_h, unsigned char out_w)
{
    // exception handling
    if(N == 0 || H == 0 || W == 0|| C == 0 || KN == 0 || KH == 0 || KW == 0)    return -1;
    else if(H < KH || W < KW)    return -1;

    // padding
    unsigned char P = pad_size;
    unsigned char* padded = (unsigned char*) malloc(N * (H + 2 * P) * (W + 2 * P) * C * sizeof(unsigned char));

    unsigned char* ptr_input = input;
    unsigned char* ptr_output = padded;

    for(int n = 0; n < N; n++) {
        for(int p = 0; p < P; p++) {
            for(int x = 0; x < (W + 2 * P); x++) {
                for(int c = 0; c < C; c++) {
                    *ptr_output++ = 0;                        // set upper row to zero
                }
            }
        }
        for(int y = P; y < (H + P); y++) {
            for(int p = 0; p < P; p++) {
                for(int c = 0; c < C; c++) {
                    *ptr_output++ = 0;                        // set left column to zero
                }
            }
            for(int x = P; x < (W + P); x++) {
                for(int c = 0; c < C; c++) {
                    *ptr_output++ = *ptr_input++;             // copy values
                }
            }
            for(int p = 0; p < P; p++) {
                for(int c = 0; c < C; c++) {
                    *ptr_output++ = 0;                        // set right column to zero
                }
            }
        }
        for(int p = 0; p < P; p++) {
            for(int x = 0; x < (W + 2 * P); x++) {
                for(int c = 0; c < C; c++) {
                    *ptr_output++ = 0;                        // set lower row to zero
                }
            }
        }
    }
    H = H + 2 * P;
    W = W + 2 * P;


    // convolution operation
    ptr_output = output;

    for(unsigned char n = 0; n < N; n++) {                                                      // for each input tensor
        for(unsigned char r_offset = 0; r_offset <= H - KH; r_offset += stride_size) {          // local area offset to compute
            for(unsigned char c_offset = 0; c_offset <= W - KW; c_offset += stride_size) {      // local area offset to compute
                for(unsigned char kn = 0; kn < KN; kn++) {                                      // for each kernel tensor
                    // for 32-bit architecture, the operation proceeds in 32 bits, 
                    // for 64-bit architecture, the operation proceeds in 64 bits.
                    INTEGER_TYPE temp = 0;

                    // compute sum of multiplication
                    for(unsigned char r_ind = 0; r_ind < KH; r_ind++) {
                        for(unsigned char c_ind = 0; c_ind < KW; c_ind++) {
                            for(unsigned char c = 0; c < C; c++) {
                                temp += *(padded + (n * H * W * C) + ((r_offset + r_ind) * W * C) + ((c_offset + c_ind) * C) + c) * *(kernel + (kn * KH * KW * C) + (r_ind * KW * C) + (c_ind * C) + c);
                            }
                        }
                    }

                    // doBias
                    if(doBias) {
                        temp += *(bias + kn);
                    }

                    // doRelu
                    if(doRelu) {
                        if(temp < 0) {
                            temp = 0;
                        }
                    }

                    // shift operation
                    temp = temp >> shift;

                    // detect underflow/overflow
                    if(temp > 255)       *(ptr_output + (n * out_h * out_w * KN) + ((r_offset / stride_size) * out_w * KN) + ((c_offset / stride_size) * KN) + kn) = 255;
                    else if(temp < 0)    *(ptr_output + (n * out_h * out_w * KN) + ((r_offset / stride_size) * out_w * KN) + ((c_offset / stride_size) * KN) + kn) = 0;
                    else                *(ptr_output + (n * out_h * out_w * KN) + ((r_offset / stride_size) * out_w * KN) + ((c_offset / stride_size) * KN) + kn) = temp;
                }
            }
        }
    }

    free(padded);
    return 0;
}

int convolution_i8_scale(unsigned char* input, float *inputScale, unsigned char *inputOffset,
                        unsigned char* kernel, float *kernelScale, unsigned char *kernelOffset,
                        unsigned char* bias, float *biasScale, unsigned char *biasOffset,
                        unsigned char* output, float* outputScale, unsigned char* outputOffset,
                        unsigned char N, unsigned char H, unsigned char W, unsigned char C,
                        unsigned char KN, unsigned char KH, unsigned char KW,
                        unsigned char pad_size, unsigned char stride_size, bool doRelu, bool doBias,
                        unsigned char out_h, unsigned char out_w)
{
    // dequantize
    float* input_fp32 = (float*) malloc(N * H * W * C * sizeof(float));
    dequantize(input, input_fp32, N * H * W * C, inputScale, inputOffset, 1, 0);
    float* kernel_fp32 = (float*) malloc(KN * KH * KW * C * sizeof(float));
    dequantize(kernel, kernel_fp32, KN * KH * KW * C, kernelScale, kernelOffset, KN, 1);
    float* bias_fp32 = (float*) malloc(KN * sizeof(float));
    dequantize(bias, bias_fp32, KN, biasScale, biasOffset, 1, 0);
    float* output_fp32 = (float*) malloc(N * out_h * out_w * KN * sizeof(float));

    // convolution operation
    convolution_fp32(input_fp32, kernel_fp32, bias_fp32, output_fp32,
                    N, H, W, C, KN, KH, KW,
                    pad_size, stride_size, doRelu, doBias,
                    out_h, out_w);
    // quantize
    quantize(output_fp32, output, N * out_h * out_w * KN, outputScale, outputOffset);

    free(input_fp32);
    free(kernel_fp32);
    free(bias_fp32);
    free(output_fp32);

    return 0;
}


int convolution_fp32(float* input, float *kernel, float *bias, float *output, 
                    unsigned char N, unsigned char H, unsigned char W, unsigned char C,
                    unsigned char KN, unsigned char KH, unsigned char KW, 
                    int pad_size, int stride_size, bool doRelu, bool doBias, 
                    unsigned char out_h, unsigned char out_w)
{
    // exception handling
    if(N == 0 || H == 0 || W == 0|| C == 0 || KN == 0 || KH == 0 || KW == 0)    return -1;
    else if(H < KH || W < KW)    return -1;

    // padding
    unsigned char P = pad_size;
    float* padded = (float*) malloc (N * (H + 2 * P) * (W + 2 * P) * C * sizeof(float));

    float* ptr_input = input;
    float* ptr_output = padded;
    
    for(int n = 0; n < N; n++) {
        for(int p = 0; p < P; p++) {
            for(int x = 0; x < (W + 2 * P); x++) {        
                for(int c = 0; c < C; c++) {
                    *ptr_output++ = 0.0;                        // set upper row to zero
                }
            }
        }
        for(int y = P; y < (H + P); y++) {
            for(int p = 0; p < P; p++) {
                for(int c = 0; c < C; c++) {
                    *ptr_output++ = 0.0;                        // set left column to zero
                }
            }
            for(int x = P; x < (W + P); x++) {
                for(int c = 0; c < C; c++) {
                    *ptr_output++ = *ptr_input++;               // copy values
                }
            }
            for(int p = 0; p < P; p++) {
                for(int c = 0; c < C; c++) {
                    *ptr_output++ = 0.0;                        // set right column to zero
                }
            }
        }
        for(int p = 0; p < P; p++) {
            for(int x = 0; x < (W + 2 * P); x++) {
                for(int c = 0; c < C; c++) {
                    *ptr_output++ = 0.0;                        // set lower row to zero
                }
            }
        }
    }
    H = H + 2 * P;
    W = W + 2 * P;

    // convolution operation
    ptr_output = output;

    for(unsigned char n = 0; n < N; n++) {                                                      // for each input tensor
        for(unsigned char r_offset = 0; r_offset <= H - KH; r_offset += stride_size) {          // area offset to compute
            for(unsigned char c_offset = 0; c_offset <= W - KW; c_offset += stride_size) {      // area offset to compute
                for(unsigned char kn = 0; kn < KN; kn++) {                                      // for each kernel tensor
                    
                    float temp = 0.0;

                    // compute sum of multiplication
                    for(unsigned char r_ind = 0; r_ind < KH; r_ind++) {
                        for(unsigned char c_ind = 0; c_ind < KW; c_ind++) {
                            for(unsigned char c = 0; c < C; c++) {
                                temp += *(padded + (n * H * W * C) + ((r_offset + r_ind) * W * C) + ((c_offset + c_ind) * C) + c) * *(kernel + (kn * KH * KW * C) + (r_ind * KW * C) + (c_ind * C) + c);
                            }
                        }
                    }

                    // doBias
                    if(doBias) {
                        temp += *(bias + kn);
                    }

                    // doRelu
                    if(doRelu) {
                        if(temp < 0) {
                            temp = 0;
                        }
                    }

                    *(ptr_output + (n * out_h * out_w * KN) + ((r_offset / stride_size) * out_w * KN) + ((c_offset / stride_size) * KN) + kn) = temp;
                }
            }
        }
    }

    free(padded);
    return 0;
}

int fullyconnected_i8_shift(unsigned char* input, unsigned char *kernel, unsigned char *bias, unsigned char *output,
                            unsigned char inputDim0, unsigned char inputDim1, unsigned char kernelDim0, unsigned char kernelDim1,
                            bool doBias, unsigned char shift, unsigned char outputDim0, unsigned char outputDim1)
{
    /*
    kernelDim0 = inputDim0 * inputDim1;
    kernelDim1 = outputDim0 * outputDim1;
    */

    int count = 0;

    unsigned char input_size = inputDim0 * inputDim1;
    unsigned char output_size = outputDim0 * outputDim1;

    // matrix multiplication
    for(int i = 0; i < output_size; i++) {
        // for 32-bit architecture, the operation proceeds in 32 bits, 
        // for 64-bit architecture, the operation proceeds in 64 bits.
        INTEGER_TYPE temp = 0;

        for(int j = 0; j < input_size; j++) {
            temp += input[j] * *(kernel + j * output_size + i);
            count++;
        }
            
        // doBias
        if(doBias) {
            temp += *bias;
        }

        // shift operation
        temp = temp >> shift;

        // overflow/underflow detection
        if(temp > 255)       output[i] = 255;
        else if(temp < 0)    output[i] = 0;
        else                output[i] = temp;
    }

    return count;
}

int fullyconnected_i8_scale(unsigned char *input, float *inputScale, unsigned char *inputOffset,
                            unsigned char *kernel, float *kernelScale, unsigned char *kernelOffset,
                            unsigned char *bias, float *biasScale, unsigned char *biasOffset,
                            unsigned char *output, float *outputScale, unsigned char *outputOffset,
                            unsigned int inputDim0, unsigned int inputDim1, 
                            unsigned int kernelDim0, unsigned int kernelDim1,
                            bool doBias, unsigned int outputDim0, unsigned int outputDim1)
{
    // dequantize
    unsigned int input_size = inputDim0 * inputDim1;
    unsigned int output_size = outputDim0 * outputDim1;
    unsigned int kernel_size = kernelDim0 * kernelDim1;
    unsigned int bias_size = inputDim0 * outputDim1;

    float* input_fp32 = (float*) malloc(input_size * sizeof(float));
    dequantize(input, input_fp32, input_size, inputScale, inputOffset, 1, 0);
    float* kernel_fp32 = (float*) malloc(kernel_size * sizeof(float));
    dequantize(kernel, kernel_fp32, kernel_size, kernelScale, kernelOffset, kernelDim1, 0);
    float* bias_fp32 = (float*) malloc(bias_size * sizeof(float));
    dequantize(bias, bias_fp32, bias_size, biasScale, biasOffset, 1, 0);
    float* output_fp32 = (float*) malloc(output_size * sizeof(float));

    // fullyconnected operation
    for(int i = 0; i < inputDim0; i++) {
        for(int j = 0; j < kernelDim1; j++) {
            output_fp32[i * kernelDim1 + j] = 0;
            for(int k = 0; k < inputDim1; k++) {
                output_fp32[i * kernelDim1 + j] += input_fp32[i * inputDim1 + k] * kernel_fp32[k * kernelDim1 + j];
            }
            if (doBias) {
                output_fp32[i * kernelDim1 + j] += bias_fp32[j];
            }
        }
    }

    // quantize
    quantize(output_fp32, output, outputDim0 * outputDim1, outputScale, outputOffset);

    free(input_fp32);
    free(kernel_fp32);
    free(bias_fp32);
    free(output_fp32);

    return 0;
}

int maxpool_i8(unsigned char* input, unsigned char *output, unsigned char N, unsigned char H, unsigned char W, unsigned char C, unsigned char KH, unsigned char KW,
               unsigned char pad_size, unsigned char stride_size)
{
    // exception handling
    if(N == 0 || H == 0 || W == 0|| C == 0 || KH == 0 || KW == 0)    return -1;
    else if(H < KH || W < KW)    return -1;

    // padding
    unsigned char P = pad_size;
    unsigned char* padded = (unsigned char*) malloc (N * (H + 2 * P) * (W + 2 * P) * C * sizeof(unsigned char));

    unsigned char* ptr_input = input;
    unsigned char* ptr_output = padded;
    
    for(int n = 0; n < N; n++) {
        for(int p = 0; p < P; p++) {
            for(int x = 0; x < (W + 2 * P); x++) {        
                for(int c = 0; c < C; c++) {
                    *ptr_output++ = 0;                        // set upper row to zero
                }
            }
        }
        for(int y = P; y < (H + P); y++) {
            for(int p = 0; p < P; p++) {
                for(int c = 0; c < C; c++) {
                    *ptr_output++ = 0;                        // set left column to zero
                }
            }
            for(int x = P; x < (W + P); x++) {
                for(int c = 0; c < C; c++) {
                    *ptr_output++ = *ptr_input++;             // copy values
                }
            }
            for(int p = 0; p < P; p++) {
                for(int c = 0; c < C; c++) {
                    *ptr_output++ = 0;                        // set right column to zero
                }
            }
        }
        for(int p = 0; p < P; p++) {
            for(int x = 0; x < (W + 2 * P); x++) {
                for(int c = 0; c < C; c++) {
                    *ptr_output++ = 0;                        // set lower row to zero
                }
            }
        }
    }
    H = H + 2 * P;
    W = W + 2 * P;

    // pooling
    ptr_output = output;

    for(int n = 0; n < N; n++) {
        for(int r_offset = 0; r_offset <= H - KH; r_offset += stride_size) {
            for(int c_offset = 0; c_offset <= W - KW; c_offset += stride_size) {
                for(int c = 0; c < C; c++) {
                    unsigned char max = 0;

                    for(int r_op = 0; r_op < KH; r_op++) {
                        for(int c_op = 0; c_op < KW; c_op++) {
                            if(*(padded + n * H * W * C + (r_offset + r_op) * W * C + (c_offset + c_op) * C + c) > max) {
                                max = *(padded + n * H * W * C + (r_offset + r_op) * W * C + (c_offset + c_op) * C + c);
                            }
                        }
                    }
                    *ptr_output++ = max;
                }
            }
        }
    }

    return 0;
}

int avgpool_i8(unsigned char* input, float *inputScale, unsigned char *inputOffset, 
            unsigned char *output, float *outputScale, unsigned char *outputOffset,
            unsigned char N, unsigned char H, unsigned char W, unsigned char C, 
            unsigned char KH, unsigned char KW, unsigned char pad_size, unsigned char stride_size)
{
    float* input_fp32 = (float*) malloc(N * H * W * C * sizeof(float));
    dequantize(input, input_fp32, N * H * W * C, inputScale, inputOffset, 1, 0);

    // padding
    unsigned char P = pad_size;
    float* padded = (float*) malloc (N * (H + 2 * P) * (W + 2 * P) * C * sizeof(float));

    float* ptr_input = input_fp32;
    float* ptr_output = padded;
    
    for(int n = 0; n < N; n++) {
        for(int p = 0; p < P; p++) {
            for(int x = 0; x < (W + 2 * P); x++) {        
                for(int c = 0; c < C; c++) {
                    *ptr_output++ = 0;                        // set upper row to zero
                }
            }
        }
        for(int y = P; y < (H + P); y++) {
            for(int p = 0; p < P; p++) {
                for(int c = 0; c < C; c++) {
                    *ptr_output++ = 0;                        // set left column to zero
                }
            }
            for(int x = P; x < (W + P); x++) {
                for(int c = 0; c < C; c++) {
                    *ptr_output++ = *ptr_input++;             // copy values
                }
            }
            for(int p = 0; p < P; p++) {
                for(int c = 0; c < C; c++) {
                    *ptr_output++ = 0;                        // set right column to zero
                }
            }
        }
        for(int p = 0; p < P; p++) {
            for(int x = 0; x < (W + 2 * P); x++) {
                for(int c = 0; c < C; c++) {
                    *ptr_output++ = 0;                        // set lower row to zero
                }
            }
        }
    }
    H = H + 2 * P;
    W = W + 2 * P;

    // pooling
    unsigned char S = stride_size;
    unsigned char H_out = (H - KH) / S + 1;
    unsigned char W_out = (W - KW) / S + 1;
    float* output_fp32 = (float*) malloc(N * H_out * W_out * C * sizeof(float));

    ptr_output = output_fp32; 

    for(int n = 0; n < N; n++) {
        for(int r_offset = 0; r_offset <= H - KH; r_offset += stride_size) {
            for(int c_offset = 0; c_offset <= W - KW; c_offset += stride_size) {
                for(int c = 0; c < C; c++) {
                    float sum = 0;
                    float average = 0;

                    for(int r_op = 0; r_op < KH; r_op++) {
                        for(int c_op = 0; c_op < KW; c_op++) {
                            sum += *(padded + n * H * W * C + (r_offset + r_op) * W * C + (c_offset + c_op) * C + c);
                        }
                    }
                    average = sum / (KH * KW);
                    *ptr_output++ = ones_round(average);
                }
            }
        }
    }

    quantize(output_fp32, output, N * H_out * W_out * C, outputScale, outputOffset);

    free(input_fp32);
    free(padded);
    free(output_fp32);

    return 0;
}

int relu(unsigned char* input, unsigned char *output, unsigned long size, unsigned char *offset)
{

    int input_adj = 0;
    for (unsigned long i = 0; i < size; ++i) {
        input_adj  = input[i] - offset[0];
        if (input_adj >= 0) {
            output[i] = input_adj + offset[0];
        }
        else {
            output[i] = offset[0];
        }
    }
    return 0;
}

int quantize(float *input, unsigned char *output, unsigned long size, float *scale, unsigned char *offset)
{
    for (unsigned long i = 0; i < size; ++i) {
        float quantized = input[i] / scale[0] + offset[0];
        if(quantized > 255) {
            output[i] = 255;
        }
        else if(quantized < 0) {
            output[i] = 0;
        }
        else {
            output[i] = (unsigned char) ones_round(quantized);
        }
    }
    return 0;
}

int dequantize(const unsigned char *input, float *output, unsigned long size,
            float *scale, unsigned char *zero_point, int channel_size, bool use_stride)
{
    if (channel_size < 1) {
        return -1;
    }
    for (unsigned long i = 0; i < size; ++i) {
        int channel_index;
        if (use_stride) {
            int stride = size / channel_size;
            channel_index = (i / stride) % channel_size;
        } else {
            if (channel_size > 1) {
                channel_index = i % channel_size;
            } else {
                channel_index = 0;
            }
        }
        output[i] = (input[i] - zero_point[channel_index]) * scale[channel_index];
    }
    return 0;
}

int transpose_i8(unsigned char *input, unsigned char *output, unsigned long inDim0, unsigned long inDim1, unsigned long inDim2, unsigned long inDim3,
                 unsigned long outDim0, unsigned long outDim1, unsigned long outDim2, unsigned long outDim3,
                 unsigned char idx0, unsigned char idx1, unsigned char idx2, unsigned char idx3) 
{
    if(inDim0 * inDim1 * inDim2 * inDim3 != outDim0 * outDim1 * outDim2 * outDim3) {
        return -1;
    }

    for (unsigned long i = 0; i < inDim0; ++i) {
        for (unsigned long j = 0; j < inDim1; ++j) {
            for (unsigned long k = 0; k < inDim2; ++k) {
                for (unsigned long l = 0; l < inDim3; ++l) {
                    unsigned long inIndex = i * inDim1 * inDim2 * inDim3 + j * inDim2 * inDim3 + k * inDim3 + l;
                    unsigned long outIndex =
                            (idx0 == 0 ? i : (idx0 == 1 ? j : (idx0 == 2 ? k : l))) * outDim1 * outDim2 * outDim3 +
                            (idx1 == 0 ? i : (idx1 == 1 ? j : (idx1 == 2 ? k : l))) * outDim2 * outDim3 +
                            (idx2 == 0 ? i : (idx2 == 1 ? j : (idx2 == 2 ? k : l))) * outDim3 +
                            (idx3 == 0 ? i : (idx3 == 1 ? j : (idx3 == 2 ? k : l)));

                    output[outIndex] = input[inIndex];
                }
            }
        }
    }

    return 0;
}

int elemadd_i8(unsigned char *input0, float *input0Scale, unsigned char *input0Offset,
            unsigned char *input1, float *input1Scale, unsigned char *input1Offset,
               unsigned char *output, float *destScale, unsigned char *destOffset, unsigned long size)
{
    if(input0Scale[0]==input1Scale[0] && input0Offset[0]==input1Offset[0] && destScale[0]==input0Scale[0] && destOffset[0]==input0Offset[0]) {
        for (unsigned long i = 0; i < size; ++i) {
            // add input0 and input1 and store in output. if overflow store 255
            if(input0[i] + input1[i] > 255) {
                output[i] = 255;
            } else {
                output[i] = input0[i] + input1[i];
            }
        }
    } else {
        float *dequantized_input0 = (float *) malloc(size * sizeof(float));
        float *dequantized_input1 = (float *) malloc(size * sizeof(float));
        float *dequantized_output = (float *) malloc(size * sizeof(float));

        dequantize(input0, dequantized_input0, size, input0Scale, input0Offset, 1, 0);
        dequantize(input1, dequantized_input1, size, input1Scale, input1Offset, 1, 0);

        for (unsigned long i = 0; i < size; ++i) {
            dequantized_output[i] = dequantized_input0[i] + dequantized_input1[i];
        }

        quantize(dequantized_output, output, size, destScale, destOffset);

        free(dequantized_input0);
        free(dequantized_input1);
        free(dequantized_output);
    }
    return 0;
}

int elemadd_relu_i8(unsigned char *input0, float *input0Scale, unsigned char *input0Offset,
                    unsigned char *input1, float *input1Scale, unsigned char *input1Offset,
                    unsigned char *output, float *destScale, unsigned char *destOffset, unsigned long size)
{
    unsigned char *output_before_ReLU = (unsigned char *) malloc(size * sizeof(unsigned char));
    
    elemadd_i8(input0, input0Scale, input0Offset, input1, input1Scale, input1Offset, output_before_ReLU, destScale, destOffset, size);
    relu(output_before_ReLU, output, size, destOffset);

    free(output_before_ReLU);
    return 0;       
}

int elemsub_i8(unsigned char *input0, float *input0Scale, unsigned char *input0Offset,
            unsigned char *input1, float *input1Scale, unsigned char *input1Offset,
            unsigned char *output, float *destScale, unsigned char *destOffset, unsigned long size)
{
    if(input0Scale[0]==input1Scale[0] && input0Offset[0]==input1Offset[0] && destScale[0]==input0Scale[0] && destOffset[0]==input0Offset[0]) {
        for (unsigned long i = 0; i < size; ++i) {
            // subtract input1 from input0 and store in output. if underflow store 0
            if(input0[i] - input1[i] < 0) {
                output[i] = 0;
            } else {
                output[i] = input0[i] - input1[i];
            }
        }
    } else {
        float *dequantized_input0 = (float *) malloc(size * sizeof(float));
        float *dequantized_input1 = (float *) malloc(size * sizeof(float));
        float *dequantized_output = (float *) malloc(size * sizeof(float));

        dequantize(input0, dequantized_input0, size, input0Scale, input0Offset, 1, 0);
        dequantize(input1, dequantized_input1, size, input1Scale, input1Offset, 1, 0);

        for (unsigned long i = 0; i < size; ++i) {
            dequantized_output[i] = dequantized_input0[i] - dequantized_input1[i];
        }

        quantize(dequantized_output, output, size, destScale, destOffset);

        free(dequantized_input0);
        free(dequantized_input1);
        free(dequantized_output);
    }
    return size;
}

int elemdiv_i8(unsigned char *input0, float *input0Scale, unsigned char *input0Offset,
            unsigned char *input1, float *input1Scale, unsigned char *input1Offset,
            unsigned char *output, float *destScale, unsigned char *destOffset, unsigned long size)
{
    if(input0Scale[0]==input1Scale[0] && input0Offset[0]==input1Offset[0] && destScale[0]==input0Scale[0] && destOffset[0]==input0Offset[0]) {
        for (unsigned long i = 0; i < size; ++i) {
            // devide input0 by input1 and store in output. there will be no overflow. if input1[i] == 0, return error code.
            if(input1[i] == 0) {
                return -1;
            } else {
                output[i] = input0[i] / input1[i];
            }
        }
    } else {
        float *dequantized_input0 = (float *) malloc(size * sizeof(float));
        float *dequantized_input1 = (float *) malloc(size * sizeof(float));
        float *dequantized_output = (float *) malloc(size * sizeof(float));

        dequantize(input0, dequantized_input0, size, input0Scale, input0Offset, 1, 0);
        dequantize(input1, dequantized_input1, size, input1Scale, input1Offset, 1, 0);


        for (unsigned long i = 0; i < size; ++i) {
            if(dequantized_input1[i] == 0) {
                return -1;
            } else {
                dequantized_output[i] = dequantized_input0[i] / dequantized_input1[i];
            }
        }

        quantize(dequantized_output, output, size, destScale, destOffset);

        free(dequantized_input0);
        free(dequantized_input1);
        free(dequantized_output);
    }
    return 0;
}

int elemmax_i8(unsigned char *input0, float *input0Scale, unsigned char *input0Offset,
            unsigned char *input1, float *input1Scale, unsigned char *input1Offset,
            unsigned char *output, float *destScale, unsigned char *destOffset, unsigned long size)
{
    if(input0Scale[0]==input1Scale[0] && input0Offset[0]==input1Offset[0] && destScale[0]==input0Scale[0] && destOffset[0]==input0Offset[0]) {
        for (unsigned long i = 0; i < size; ++i) {
            if(input0[i] > input1[i]) {
                output[i] = input0[i];
            } else {
                output[i] = input1[i];
            }
        }
    } else {
        float *dequantized_input0 = (float *) malloc(size * sizeof(float));
        float *dequantized_input1 = (float *) malloc(size * sizeof(float));
        float *dequantized_output = (float *) malloc(size * sizeof(float));

        dequantize(input0, dequantized_input0, size, input0Scale, input0Offset, 1, 0);
        dequantize(input1, dequantized_input1, size, input1Scale, input1Offset, 1, 0);

        for (unsigned long i = 0; i < size; ++i) {
            if(dequantized_input0[i] > dequantized_input1[i]) {
                dequantized_output[i] = dequantized_input0[i];
            } else {
                dequantized_output[i] = dequantized_input1[i];
            }
        }

        quantize(dequantized_output, output, size, destScale, destOffset);

        free(dequantized_input0);
        free(dequantized_input1);
        free(dequantized_output);
    }
    return size;
}

int splat_i8(unsigned char *input, unsigned long size, unsigned char value)
{
    for (unsigned long i = 0; i < size; ++i) {
        input[i] = value;
    }
    return size * sizeof(value);
}

int softmax(unsigned char *input, float *output, unsigned long inDim0, unsigned long inDim1, unsigned long outDim0, unsigned long outDim1) {
    for (unsigned long i = 0; i < inDim0; i++) {
        for (unsigned long j = 0; j < inDim1; j++) {
            unsigned long index_in = i * inDim1 + j;
            unsigned long index_out = i * outDim1 + j;

            // Find the maximum input value in the row
            float max_val = (float)input[i * inDim1];
            for (unsigned long k = 1; k < inDim1; k++) {
                float current_val = (float)input[i * inDim1 + k];
                if (current_val > max_val) {
                    max_val = current_val;
                }
            }

            // Compute exponentials using a more accurate Taylor series approximation
            float x = (float)input[index_in] - max_val; // Subtract max value
            float exp_value = ones_exp(x, 64);  // Increase the number of terms for better accuracy
//            float exp_value2 = exp(x);
//            printf("%f, %f \n", exp_value, exp_value2 );

            // Sum of exponentials for normalization
            float exp_sum = 0.0f;
            float exp_sum2 = 0.0f;
            for (unsigned long k = 0; k < inDim1; k++) {
                unsigned long index_k = i * inDim1 + k;
                exp_sum += ones_exp((float)input[index_k] - max_val, 64);  // Increase the number of terms for better accuracy
 //               exp_sum += exp((float)input[index_k] - max_val);
 //               printf("%f, %f", exp_sum, exp_sum2 );
            }

            // Compute softmax output
            output[index_out] = exp_value / exp_sum;
            // printf("%e ", output[index_out]);
        }
    }
    return 0;
}
