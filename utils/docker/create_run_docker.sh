#!/bin/bash

if [[ $# -ne 2 ]]
then
    echo "Direction: ./create_run_docker.sh SHARED_DIR CONTAINER_NAME"
    exit
fi


export TEST_HOME=$PWD/$1


docker build  -t arx_api_test $TEST_HOME/utils/docker/
docker run -it -v $TEST_HOME:/root/dev --name=$2 arx_api_test /bin/bash